package net.buubuulle.simplehammer.events;

import net.buubuulle.simplehammer.Main;
import net.minecraft.server.v1_16_R2.BlockPosition;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Hammer implements Listener {
    private Main plugin;

    public Hammer(Main plugin) {
        this.plugin = plugin;
        lastFace = new HashMap<>();
        inBreaking = new ArrayList<>();
    }

    HashMap<Player, BlockFace> lastFace;
    List<Player> inBreaking;

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        ItemStack item = p.getInventory().getItemInMainHand();
        ItemMeta itemM = (item.hasItemMeta()) ? item.getItemMeta() : null;
        if (!p.hasPermission("simplehammer.hammer.use")) return;
        if (inBreaking.contains(p)) {
            if(e.getBlock().getType() != Material.AIR) {
                ItemStack tempItem = p.getInventory().getItemInMainHand();
                ItemMeta tempMeta = tempItem.getItemMeta();
                if (tempMeta instanceof Damageable) {
                    ((Damageable) tempMeta).setDamage(((Damageable) tempMeta).getDamage() - 1);
                }
                tempItem.setItemMeta(tempMeta);
                p.getInventory().setItemInMainHand(tempItem);
            }
            return;
        }
        if (itemM != null && itemM.getLore() != null && itemM.getLore().contains("§cHammer")) {
            BlockFace face = lastFace.get(p);
            ArrayList<Block> blocks = new ArrayList<>();
            if (face.equals(BlockFace.UP) || face.equals(BlockFace.DOWN)) {
                blocks = getBlocks(e.getBlock(), 1, 0, 1);
            } else if (face.equals(BlockFace.NORTH) || face.equals(BlockFace.SOUTH)) {
                blocks = getBlocks(e.getBlock(), 1, 1, 0);
            } else if (face.equals(BlockFace.WEST) || face.equals(BlockFace.EAST)) {
                blocks = getBlocks(e.getBlock(), 0, 1, 1);
            }
            inBreaking.add(p);
            for (Block block : blocks) {
                Collection<ItemStack> items = block.getDrops(p.getInventory().getItemInMainHand());
                if (items.size() > 0) {
                    if (e.getBlock().getType().getHardness() + 3 >= block.getType().getHardness()) {
                        ItemStack tempItem = p.getInventory().getItemInMainHand();
                        ItemMeta tempMeta = tempItem.getItemMeta();
                        if (tempMeta instanceof Damageable) {
                            ((Damageable) tempMeta).setDamage(((Damageable) tempMeta).getDamage() + 1);
                        }
                        tempItem.setItemMeta(tempMeta);
                        p.getInventory().setItemInMainHand(tempItem);
                        ((CraftPlayer) e.getPlayer()).getHandle().playerInteractManager.breakBlock(new BlockPosition(block.getX(), block.getY(), block.getZ()));
                    }
                }
            }
            inBreaking.remove(p);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (!lastFace.containsKey(p)) {
                lastFace.put(p, e.getBlockFace());
                return;
            }
            lastFace.put(p, e.getBlockFace());
        }
    }

    public ArrayList<Block> getBlocks(Block start, int xRadius, int yRadius, int zRadius) {
        ArrayList<Block> blocks = new ArrayList<Block>();
        for (int x = start.getLocation().getBlockX() - xRadius; x <= start.getLocation().getBlockX() + xRadius; x++) {
            for (int y = start.getLocation().getBlockY() - yRadius; y <= start.getLocation().getBlockY() + yRadius; y++) {
                for (int z = start.getLocation().getBlockZ() - zRadius; z <= start.getLocation().getBlockZ() + zRadius; z++) {
                    Location loc = new Location(start.getWorld(), x, y, z);
                    blocks.add(loc.getBlock());
                }
            }
        }
        return blocks;
    }
}
