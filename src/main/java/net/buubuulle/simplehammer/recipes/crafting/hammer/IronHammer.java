package net.buubuulle.simplehammer.recipes.crafting.hammer;

import net.buubuulle.simplehammer.Main;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class IronHammer {
    private Main plugin;

    public IronHammer(Main plugin) {
        this.plugin = plugin;
    }

    public void setupRecipe() {
        ItemStack hammer = plugin.getItem(Material.IRON_PICKAXE, 1, "Iron hammer", Arrays.asList("§cHammer", "§c3x3"));
        ShapedRecipe recipe = new ShapedRecipe(hammer);

        recipe.shape("III", "ISI", " S ");
        recipe.setIngredient('I', Material.IRON_INGOT);
        recipe.setIngredient('S', Material.STICK);

        plugin.getServer().addRecipe(recipe);
    }
}
