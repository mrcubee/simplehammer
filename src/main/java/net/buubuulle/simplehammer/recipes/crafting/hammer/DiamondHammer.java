package net.buubuulle.simplehammer.recipes.crafting.hammer;

import net.buubuulle.simplehammer.Main;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class DiamondHammer {
    private Main plugin;

    public DiamondHammer(Main plugin) {
        this.plugin = plugin;
    }

    public void setupRecipe() {
        ItemStack hammer = plugin.getItem(Material.DIAMOND_PICKAXE, 1, "Diamond hammer", Arrays.asList("§cHammer", "§c3x3"));
        ShapedRecipe recipe = new ShapedRecipe(hammer);

        recipe.shape("DDD", "DSD", " S ");
        recipe.setIngredient('D', Material.DIAMOND);
        recipe.setIngredient('S', Material.STICK);

        plugin.getServer().addRecipe(recipe);
    }
}
