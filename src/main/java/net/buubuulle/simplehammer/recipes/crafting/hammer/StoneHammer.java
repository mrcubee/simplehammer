package net.buubuulle.simplehammer.recipes.crafting.hammer;

import net.buubuulle.simplehammer.Main;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class StoneHammer {
    private Main plugin;

    public StoneHammer(Main plugin) {
        this.plugin = plugin;
    }

    public void setupRecipe() {
        ItemStack hammer = plugin.getItem(Material.STONE_PICKAXE, 1, "Stone hammer", Arrays.asList("§cHammer", "§c3x3"));
        ShapedRecipe recipe = new ShapedRecipe(hammer);

        recipe.shape("CCC", "CSC", " S ");
        recipe.setIngredient('C', Material.COBBLESTONE);
        recipe.setIngredient('S', Material.STICK);

        plugin.getServer().addRecipe(recipe);
    }
}
