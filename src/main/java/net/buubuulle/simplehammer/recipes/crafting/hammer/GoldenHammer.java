package net.buubuulle.simplehammer.recipes.crafting.hammer;

import net.buubuulle.simplehammer.Main;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class GoldenHammer {
    private Main plugin;

    public GoldenHammer(Main plugin) {
        this.plugin = plugin;
    }

    public void setupRecipe() {
        ItemStack hammer = plugin.getItem(Material.GOLDEN_PICKAXE, 1, "Golden hammer", Arrays.asList("§cHammer", "§c3x3"));
        ShapedRecipe recipe = new ShapedRecipe(hammer);

        recipe.shape("GGG", "GSG", " S ");
        recipe.setIngredient('G', Material.GOLD_INGOT);
        recipe.setIngredient('S', Material.STICK);

        plugin.getServer().addRecipe(recipe);
    }
}
