package net.buubuulle.simplehammer;


import net.buubuulle.simplehammer.events.Hammer;
import net.buubuulle.simplehammer.recipes.crafting.hammer.DiamondHammer;
import net.buubuulle.simplehammer.recipes.crafting.hammer.GoldenHammer;
import net.buubuulle.simplehammer.recipes.crafting.hammer.IronHammer;
import net.buubuulle.simplehammer.recipes.crafting.hammer.StoneHammer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class Main extends JavaPlugin {
    //Crafts
    public DiamondHammer diamondHammer;
    public GoldenHammer goldenHammer;
    public IronHammer ironHammer;
    public StoneHammer stoneHammer;
    //Events
    public Hammer hammer;


    @Override
    public void onEnable() {
        registerClasses();
        registerEvents();
        registerRecipes();
    }

    public void registerClasses() {
        //Crafts
        diamondHammer = new DiamondHammer(this);
        goldenHammer = new GoldenHammer(this);
        ironHammer = new IronHammer(this);
        stoneHammer = new StoneHammer(this);
        //Events
        hammer = new Hammer(this);
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(hammer, this);
    }

    private void registerRecipes() {
        diamondHammer.setupRecipe();
        goldenHammer.setupRecipe();
        ironHammer.setupRecipe();
        stoneHammer.setupRecipe();
    }

    public ItemStack getItem(Material material, int count, String name, List<String> lore) {
        ItemStack item = new ItemStack(material, Math.min(count, material.getMaxStackSize()));
        ItemMeta itemM = item.getItemMeta();
        if (itemM != null) {
            if (name != null) itemM.setDisplayName(name);
            if (lore != null) itemM.setLore(lore);
        }
        item.setItemMeta(itemM);
        return item;
    }
}
